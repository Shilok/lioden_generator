<?php

// Function to get style from CSS in HTML (https://stackoverflow.com/questions/41480904/how-to-find-the-style-property-of-dom-element-using-dom-document-in-php)
// https://tonyspiro.com/using-php-to-get-a-string-between-two-strings/
function getStyle($string, $start, $end)
{
    $ini = explode($start, $string);
    if (isset($ini[1]))
    {
        $ini = explode($end, $ini[1]);
        return $ini[0];
    }
    return '';
}

// Get link and checkbox state
$url = $_POST['url'];
$decorations = $_POST['decorations'];
$background = $_POST['background'];

// Take URL content
$html = file_get_contents($url);

// Get image tags using DOM elements and div tags for BG
$dom = new DOMDocument;
@$dom->loadHTML($html);
$imgs = $dom->getElementsByTagName('img');

// Array of pictures
$images = [];

// Fetch the decors if checked, lion pictures, and put them in the array
foreach ($imgs as $img)
{
    $fetchedLayer = array(
        'src' => $img->getAttribute('src'),
        'opacity' => getStyle($img->getAttribute('style'), 'opacity:', ';')
    );

    // Include background if checked
    if (isset($background) && strpos($fetchedLayer['src'], 'backgrounds'))
    {
        $images[] = $fetchedLayer;
    }

    // Include decorations if checked
    if (isset($decorations) && strpos($fetchedLayer['src'], 'decors'))
    {
        $images[] = $fetchedLayer;
    }

    // Include whole lion/lioness
    if (strpos($fetchedLayer['src'], 'lion'))
    {
        $images[] = $fetchedLayer;
    }
}

// If no images are fetched
if (empty($images))
{
    echo 'No images found! Enter an URL of a lion or lioness.';
}
else
{
    // Create the transparent base of the final picture
    $width = 640;
    $height = 500;
    $lionPicture = imagecreatetruecolor($width, $height);
    imagesavealpha($lionPicture, true); // Allows alpha save-state
    $transparent = imagecolorallocatealpha($lionPicture, 0, 0, 0, 127); // 127 = transparent, 0 = opaque
    imagefill($lionPicture, 0, 0, $transparent);
    
    // Add every image with their opacity in the final picture
    foreach ($images as $image)
    {
        $layer = imagecreatefrompng('https:' . $image['src']);
        if (!$image['opacity'])
        {
            $image['opacity'] = 1;
        }
        $opacity = 127 - (127 * $image['opacity']);
        imagealphablending($layer, false); // Needed for imagesavealpha to work
        imagesavealpha($layer, true); // Needed to retain opacity
        imagefilter($layer, IMG_FILTER_COLORIZE, 0, 0, 0, $opacity);
        imagecopy($lionPicture, $layer, 0, 0, 0, 0, $width, $height);
    }
    
    // Generate picture (https://stackoverflow.com/questions/22583061/getting-imagepng-in-ajax-response)
    ob_start();
    imagepng($lionPicture);
    $imageData = ob_get_contents();
    ob_end_clean();
    $encodedPicture = base64_encode($imageData);
    echo '<img src="data:image/png;base64, ' . $encodedPicture . '">';
}