$(document).ready(function()
{
    $('#submit').click(function() {

        // URL verifications
        var str = 'lioden';
        var url = $('#url').val();

        if (url === '')
        {
            $('#output').html('<p>You have to enter an URL.</p>');
        }
        else if (!url.match(str))
        {
            $('#output').html('<p>You have to enter a lioden URL.</p>');
        }
        else
        {
            // Loading
            $('#submit, #reload').prop('disabled', true).css("background-color", "#BDC4BD");
            $('#output').html('<img src="http://www.cendrelune.fr/shilok/images/loading.gif"><br><p>It can take up to 1 minute!</p><p>Be patient! ;)</p>');

            // Sending request
            $.ajax({
                type: 'POST',
                url: 'generator.php',
                data: $('form').serialize(),
                success: function (data) {
                    $('#output').html(data);
                    $('#submit, #reload').prop('disabled', false).css("background-color", "#E66935");
                },
                error: function (error) {
                    $('#output').html('<p>An error occurred!</p><p>Since that\'s not supposed to happen, <a href="mailto:shilok@cendrelune.fr">please contact me!</a> :)</p>');
                }
            });
        }
    });

    $('#reload').click(function() {
        location.reload();
    });
});